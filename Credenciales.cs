﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.ConsumoReportes.Credenciales
// Assembly: AgentesExternos.ConsumoReportes, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B90F9E17-B8C2-48BD-A992-BE65C8F56915
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.ConsumoReportes.dll

using Microsoft.Reporting.WebForms;
using System.Net;
using System.Security.Principal;

namespace AgentesExternos.ConsumoReportes
{
  public class Credenciales : IReportServerCredentials
  {
    private readonly string _contrasena;
    private readonly string _dominio;
    private readonly string _usuario;

    public Credenciales(string usuario, string contrasena, string dominio)
    {
      this._usuario = usuario;
      this._contrasena = contrasena;
      this._dominio = dominio;
    }

    public WindowsIdentity ImpersonationUser
    {
      get
      {
        return (WindowsIdentity) null;
      }
    }

    public ICredentials NetworkCredentials
    {
      get
      {
        return (ICredentials) new NetworkCredential()
        {
          Domain = this._dominio,
          Password = this._contrasena,
          UserName = this._usuario
        };
      }
    }

    public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
    {
      authCookie = (Cookie) null;
      userName = password = authority = (string) null;
      return false;
    }
  }
}
