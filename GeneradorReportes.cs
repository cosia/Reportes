﻿// Decompiled with JetBrains decompiler
// Type: AgentesExternos.ConsumoReportes.GeneradorReportes
// Assembly: AgentesExternos.ConsumoReportes, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B90F9E17-B8C2-48BD-A992-BE65C8F56915
// Assembly location: C:\Users\cristian.osia\Desktop\CCB\AgentesExternos.ConsumoReportes.dll

using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;

namespace AgentesExternos.ConsumoReportes
{
  public class GeneradorReportes
  {
    public void GenerarPdfParaOrdenDeCompra(string numeroLiquidacion, string urlServidor, string rutaReporte, string rutaDeEscritura, string prefijo, string credencialUsuario, string credencialContrasena, string credencialDominio)
    {
      ReportViewer reportViewer = new ReportViewer()
      {
        ProcessingMode = ProcessingMode.Remote
      };
      reportViewer.ServerReport.ReportServerUrl = new Uri(urlServidor);
      reportViewer.ServerReport.DisplayName = numeroLiquidacion;
      reportViewer.ServerReport.ReportPath = rutaReporte;
      reportViewer.ServerReport.ReportServerCredentials = (IReportServerCredentials) new Credenciales(credencialUsuario, credencialContrasena, credencialDominio);
      reportViewer.ServerReport.SetParameters((IEnumerable<ReportParameter>) new List<ReportParameter>()
      {
        new ReportParameter(nameof (numeroLiquidacion), numeroLiquidacion)
      });
      string mimeType;
      string encoding;
      string fileNameExtension;
      string[] streams;
      Warning[] warnings;
      byte[] buffer = reportViewer.ServerReport.Render("PDF", (string) null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
      string path = string.Format("{0}/{1}{2}.pdf", (object) rutaDeEscritura, (object) prefijo, (object) numeroLiquidacion);
      if (File.Exists(path))
        File.Delete(path);
      using (FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
      {
        fileStream.Write(buffer, 0, buffer.Length);
        fileStream.Close();
      }
    }
  }
}
